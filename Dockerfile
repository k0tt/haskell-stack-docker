FROM haskell:8.6.3

# Update system packages
RUN apt-get update

# Install xz, make
RUN apt-get install -y make xz-utils

# Upgrade stack
RUN stack upgrade
