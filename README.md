haskell-stack
=============

GHC version: 8.6.5

Stack version: 1.9.3

[Haskell Stack Documentation](https://docs.haskellstack.org/en/stable/README/)
